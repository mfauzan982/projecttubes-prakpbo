import java.util.*;
import user.*;

import content.*;

public class MainProgram {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int choice;
        boolean exit = true;
        String back = "t";

        // Membuat objek subscription dengan mengirim argumen yang berisi user
        DashboardUser user1 = new DashboardUser("fauzan982", "1234", 0);

        // Membuat objek content dengan mengirim argumen yang berisi user
        ContentDashboard watchFilm = new ContentDashboard(user1);

        do {
            // do while pertama untuk menampilkan dashboard login
            clearScreen();
            System.out.println("Welcome to netflix");
            user1.loginMenu();
            do {
                // do while kedua untuk menampilkan menu applikasi
                clearScreen();
                System.out.println("1. Check Subscription\n2. Buy Package\n3. Watch\n4. Exit");
                System.out.print("Enter your choice : ");
                choice = userInput.nextInt();
                switch (choice) {
                    case 1:
                        user1.checkSubscription();
                        break;
                    case 2:
                        user1.buySubscription();
                        break;
                    case 3:
                        watchFilm.contentMenu();
                        break;
                    case 4:
                        exit = false;
                        break;
                    default:
                        System.out.println("Your choice is not available!");
                        break;
                }
                if (choice != 4) {
                    System.out.print("Want to back? ");
                    back = userInput.next();
                }

            } while (exit || back.equalsIgnoreCase("y"));
        } while (true);
    }

    public static void clearScreen() {
        // method yang digunakan agar terminal bersih
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
