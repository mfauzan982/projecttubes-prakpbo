package user;

public class DashboardUser extends User {
    public DashboardUser(String username, String password, int month) {
        super(username, password, month);
    }

    @Override
    public void loginMenu() {
        System.out.println("\nLogin to your account");
        System.out.print("Username : ");
        String usernameInput = userInput.next();
        System.out.print("Password : ");
        String passInput = userInput.next();

        if (usernameInput.equals(getUser()) && passInput.equals(getPassword())) {
            System.out.println("\nLogin Success\n");
            cek = false;
        } else {
            while (cek) {
                System.out.println("Username/Password is Wrong!");
                // jika user salah memasukkan data => tampil menu awal kembali
                this.loginMenu();
            }
        }
    }

    // method untuk cek user sudah member/belum
    @Override
    public void checkSubscription() {
        System.out.print("Do you want to check your subscription? ");
        char yesOrNo = userInput.next().charAt(0);

        if (yesOrNo == 'y') {
            if (getMonth() > 0) {
                System.out.println("You still have " + getMonth() + " month subscription");
            } else {
                System.out.println("You do not have a package, please buy first ");
            }
        }
    }

    // method agar user dapat memiliki paket
    @Override
    public void buySubscription() {
        System.out.println("Packages available");
        System.out.println("1 Month\n2 Month\n3 Month");
        System.out.print("Please choose the package you want ");
        int packageChoice = userInput.nextInt();
        switch (packageChoice) {
            case 1:
                setMonth(1);
                break;
            case 2:
                setMonth(2);
                break;
            case 3:
                setMonth(3);
                break;
            default:
                System.out.println("Your choice is not available!");
        }
        if (packageChoice <= 3) {
            System.out.println("You have purchased " + packageChoice + " month plan");
        }
    }
}
