package user;

import java.util.Scanner;

public abstract class User {
    Scanner userInput = new Scanner(System.in);

    private String username;
    private String password;
    protected int month;
    boolean cek = true;

    public User(String username, String password, int month) {
        this.username = username;
        this.password = password;
        this.month = month;
    }

    public String getUser() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMonth() {
        return month;
    }

    public abstract void loginMenu();

    public abstract void checkSubscription();

    public abstract void buySubscription();
}