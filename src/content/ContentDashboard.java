package content;

import java.util.*;
import user.DashboardUser;

public class ContentDashboard extends Database {
    Scanner userInput = new Scanner(System.in);
    DashboardUser user;

    public ContentDashboard(DashboardUser user) {
        this.user = user;
    }

    public void contentChoice() {
        addDataFilm();
        addDataSeries();
        int i = 1;
        for (Film film : this.film) {
            System.out.println(i + ". " + film.getTitle());
            i++;
        }
        for (Series series : this.series) {
            System.out.println(i + ". " + series.getTitle());
            i++;
        }
    }

    public void contentMenu() {
        // menampilkan menu content
        do {
            // jika user berlangganan maka dapat memilih film
            if (user.getMonth() > 0) {
                System.out.println("Film Collection: ");
                contentChoice();
                System.out.print("What do you want to watch? ");
                int choice = userInput.nextInt();

                display(choice);
                System.out.print("Want to play? ");
                String nextStep = userInput.next();

                if (nextStep.equalsIgnoreCase("y")) {
                    System.out.println("Enjoy!");
                    break;
                }
                // user tidak dapat memilih film jika tidak berlangganan
            } else {
                System.out.println("You do not have a package, please buy first");
                break;
            }
        } while (true);
    }

    // method untuk menampilkan film pilihan user
    public void display(int userChoice) {
        System.out.println("You will watch: ");
        if (userChoice <= 2) {
            Film film = this.film.get(userChoice - 1);
            System.out.println("Title: " + film.getTitle());
            System.out.println("Release Year: " + film.getReleaseYear());
            System.out.println("Duration: " + film.getDuration());
        } else {
            int option = userChoice - film.size();
            Series series = this.series.get(option - 1);
            System.out.println("Title: " + series.getTitle());
            System.out.println("Release Year: " + series.getReleaseYear());
            System.out.println(series.getSeason());
            System.out.println(series.getEpisode());
        }
    }
}
