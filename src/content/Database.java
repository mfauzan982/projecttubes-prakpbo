package content;

import java.util.*;

public class Database {

    ArrayList<Film> film = new ArrayList<>();
    ArrayList<Series> series = new ArrayList<>();

    public void addDataFilm() {
        Film film1 = new Film("Mencuri Raden Saleh", "2022", "2 Jam 32 Menit");
        Film film2 = new Film("F9 Fast Saga", "2021", "2 Jam 22 Menit");

        film.add(film1);
        film.add(film2);
    }

    public void addDataSeries() {
        Series series1 = new Series("Stranger Things", "2016", "4 Season", "8 Episode");
        Series series2 = new Series("Breaking Bad", "2008", "5 Season", "7 Episode");

        series.add(series1);
        series.add(series2);
    }
}
