package content;

public class Series extends DataContent {
    String season;
    String episode;

    public Series(String title, String releaseYear, String season, String episode) {
        super(title, releaseYear);
        this.season = season;
        this.episode = episode;
    }

    public String getSeason() {
        return season;
    }

    public String getEpisode() {
        return episode;
    }

}
