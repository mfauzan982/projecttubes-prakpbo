package content;

public class DataContent {
    String title;
    String releaseYear;

    public DataContent(String title, String releaseYear) {
        this.title = title;
        this.releaseYear = releaseYear;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseYear() {
        return releaseYear;
    }
}
