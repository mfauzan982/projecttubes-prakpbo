package content;

import java.util.*;

public class Film extends DataContent {
    Scanner userInput = new Scanner(System.in);
    String duration;

    public Film(String title, String releaseYear, String duration) {
        super(title, releaseYear);
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }
}
