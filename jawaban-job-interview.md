# No 1
Pada program ini terdapat method setMonth yang berfungsi ketika pengguna memilih paket
![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/1.gif)

# No 2
Pada program ini terdapat method checkSubscription yang berfungsi agar user dapat mengecek status subscription nya. Lalu didalam nya memanggil method getMonth yang berfungsi untuk mengambil nilai yang menunjukan berapa lama subscription itu. Saya menggunakan if else untuk menampilkan apakah user sudah berlangganan atau belum.
![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/2.gif)

# No 3
OOP mempunyai 4 pilar penting yaitu:
1. Inheritance 
Inheritance adalah suatu konsep oop dimana suatu class dapat menurunkan sifat nya ke class lain. Pada beberapa bahasa pemrograman inheritance ditandai dengan adanya kata kunci "extends", yang berarti class tersebut merupakan turunan dari class lainnya dan semua hal yang ada di class parent nya akan dimiliki oleh child class nya.
2. Abstrak
Abstrak digunakan untuk menyembunyikan detail atau sesuatu yang tidak relevan. Suatu class abstrak tidak bisa dijadikan objek seperti class lainnya. Method abstrak hanya bisa digunakan oleh turunan nya dan itu berarti meng-override method yang ada di parent.
3. Polymorphism
Polymorphism memungkinkan untuk menggunakan method dengan nama yang sama tetapi dengan sifat atau fungsi yang berbeda. Polymorphism ada 2 yaitu, overriding dan overloading. 
4. Encapsulation
Encapsulation di bahasa java ada 3 yaitu, publik, private, dan protected. Suatu variabel, atau method yang diberi kata kunci publik akan bisa di akses dimanapun. Private akan membuat method dan variabel hanya bisa diakses di kelas nya sendiri. Protected akan membuat method dan variabel di akses oleh child class nya.

# No 4
Pada class berikut terdapat variabel yang diberi kata kunci private, pemberian private pada variabel dimaksudkan agar username dan password tidak bisa di akses sembarangan di class manapun karena 2 hal tersebut merupakan hal penting.
![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/4.gif)

# No 5
Class ini merupakan abstract class dengan beberapa method abstract. Class ini tidak bisa diinisialisasi secara langsung dan merupakan superclass yang akan mewarisi semua method yang ada. Class User disini menjadi wadah yang nantinya child class User akan mengambil sifat parent nya.
![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/5.gif)

# No 6
Class ini merupakan turunan dari class User yang merupakan class abstract. Di class ini terdapat method yang meng-override method abstract yang ada di class User.
![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/6.gif)

# No 7
1.	Pengguna dapat membuka netflix.
2.	Pengguna dapat login ke akun netflix nya dengan memasukkan username dan password.
3.	Pengguna dapat mengubah foto profile sesuai yang diinginkan.
4.	Applikasi memvalidasi username dan password pengguna. Jika salah satu data salah maka akan diminta memasukkan data lagi.
5.	Jika user lupa password, maka akan ada pilihan “forget password”, yang nantinya akan dikirim kode untuk reset password.
6.	Jika login berhasil akan tampil halaman utama.
7.	Pengguna dapat mengecek apakah masih berlangganan paket atau tidak.
8.	Pengguna yang tidak memiliki paket, bisa memilih 3 pilihan paket yang tersedia.
9.	Pengguna yang memiliki paket dapat membuka halaman content untuk memilih content yang ingin ditonton.
10.	Pengguna dapat mencari content yang ingin ditonton dengan mengetikan judul.
11.	Pengguna yang tidak memiliki paket, tidak bisa mengakses halaman content dan perlu membeli paket terlebih dahulu.
12.	Pengguna dapat memilih content yang ingin di tontonnya.
13.	Pengguna dapat melihat detail film atau series yang ingin ditonton, seperti judul,tahun rilis,durasi,season,episode.
14.	Pengguna dapat mulai menonton content yang dipilihnya.
15.	Pengguna dapat menambahkan content kedalam daftar tonton.
16.	Pengguna dapat kembali ke halaman awal.
17.	Pengguna dapat logout dari akunnya.

# No 8
![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/Class_diagram_netlix.drawio.png)
**Use Case Sisi User**
| No | Use Case | Prioritas | Status |
|----|----------|-----------|--------|
|1|Pengguna dapat membuka aplikasi Netflix|Tinggi|Completed|
|2|	Pengguna dapat mengakses konten Netflix melalui berbagai platform, termasuk smartphone, tablet, Smart TV, dan komputer.|Sedang|Completed
|3|Aplikasi menampilkan halaman login.|Tinggi|Completed|
|4|Jika tidak memiliki akun, pengguna dapat membuat akun terlebih dahulu.|Tinggi|Completed|
|5|Jika pengguna memiliki pertanyaan ketika login, pengguna dapat mengakses fitur “Need help”.|Sedang|Uncompleted
|6|Pengguna memasukkan username dan password mereka.|Tinggi|Completed|
|7|Pengguna dapat menggunakan fitur forget password jika lupa password.|Sedang|Unclompeted|
|8|Jika login berhasil, aplikasi menampilkan halaman utama atau dashboard.|Tinggi|Completed
|9|Jika memiliki paket Premium pengguna dapat memiliki hingga 5 profile untuk 1 akun netflix mereka.|Sedang|Completed|
|10|Pengguna dapat berpindah-pindah profile.|Sedang|Unclompeted|
|11|Pengguna dapat mengunci tiap profile dengan 4 angka pin yang mereka tetapkan.|Sedang|Unclompeted|
|12|Pengguna dapat mengatur profile picture tiap profile dengan berbagai macam pilihan yang disediakan.|Rendah|Unclompeted|
|13|Pengguna dapat memilih bahasa yang digunakan untuk dashboard netflix.|Rendah|Unclompeted|
|14|Pengguna dapat melihat daftar film dan serial TV yang tersedia.|Tinggi|Unclompeted|
|15|Pengguna dapat mencari film atau serial TV berdasarkan judul atau kategori.|Sedang|Unclompeted|
|16|Pengguna dapat melihat detail film atau serial TV tertentu, termasuk judul, tahun rilis, durasi, sinopsis, dan rating.|Sedang|Completed
|17|Pengguna dapat menambahkan film atau serial TV ke daftar tontonan atau favorit.|Sedang|Completed|
|18|Pengguna dapat menonton film atau serial TV yang dipilih.|Tinggi|Unclompeted
|19|Menandai film/serial TV sebagai "Favorit" untuk referensi cepat di masa mendatang.|Sedang|Unclompeted|
|20|Menyembunyikan judul film/serial TV dari rekomendasi dengan fitur "Sembunyikan dari Rekomendasi".|Rendah|Unclompeted|
|21|Jika pengguna memilih menonton, aplikasi menampilkan halaman pemutaran.|Tinggi|Unclompeted|
|22|Pengguna dapat memutar, menjeda, atau menghentikan pemutaran.|Sedang|Unclompeted|
|23|Pengguna dapat menonton film/serial TV dengan kualitas streaming yang tertinggi jika memiliki paket 4K+.|Sedang|Unclompeted|
|24|Menggunakan fitur "Continue Watching" untuk melanjutkan menonton di perangkat yang berbeda dengan mudah.|Sedang|Unclompeted|
|25|Pengguna dapat melanjutkan menonton dari titik terakhir yang ditinggalkan.|Sedang|Unclompeted|
|26|Pengguna dapat mengunduh film/serial TV untuk ditonton secara offline.|Sedang|Unclompeted|
|27|Memberikan rating dan ulasan untuk film/serial TV yang ditonton.|Rendah|Unclompeted|
|28|Mengatur preferensi konten untuk mendapatkan rekomendasi yang lebih sesuai.|Rendah|Unclompeted
|29|Mengatur pengaturan kontrol orang tua untuk membatasi akses anak-anak ke konten yang tidak sesuai.|Rendah|Unclompeted|
|30|Mengatur profil pengguna terpisah dengan preferensi dan rekomendasi yang dipersonalisasi.|Rendah|Unclompeted|
|31|Menonton trailer dan cuplikan untuk mendapatkan gambaran tentang konten yang akan ditonton.|Rendah|Completed|
|32|Menonton film/serial TV dalam mode "Maraton" secara berurutan tanpa interupsi.|Rendah|Unclompeted
|33|Menggunakan fitur "Skip Intro" untuk melewati bagian intro yang berulang di serial TV.|Rendah|Unclompeted|
|34|Menggunakan fitur "Skip Recap": Melewati bagian pengulangan adegan sebelum episode baru dalam serial TV yang sedang ditonton.|Rendah|Unclompeted
|35|Menonton konten Netflix dengan audio deskripsi untuk pengalaman menonton yang inklusif bagi pengguna dengan kebutuhan visual.|Sedang|Unclompeted
|36|Jika pengguna memiliki paket tertentu, pengguna dapat menonton film/serial TV dengan audio multi-channel atau Dolby Atmos untuk pengalaman suara yang lebih mendalam.|Tinggi|Unclompeted|
|37|Pengguna dapat mengatur pengaturan "Data Usage" untuk mengontrol penggunaan data saat menonton di perangkat seluler.|Sedang|Unclompeted
|38|Setelah menonton selesai, pengguna dapat menutup halaman pemutaran dan kembali ke halaman utama.|Sedang|Unclompeted|
|39|Menggunakan fitur "Actor/Actress Filmography" untuk menjelajahi filmografi dari aktor/aktris yang terlibat dalam film/serial TV tertentu.|Rendah|Unclompeted
|40|Menggunakan fitur "Smart Recommendations" untuk mendapatkan rekomendasi konten yang disesuaikan berdasarkan tren saat ini dan popularitas di kalangan pengguna Netflix lainnya.|Rendah|Unclompeted
|41|Pengguna dapat mengakses setting aplikasi untuk menyesuaikan kebutuhan.|Rendah|Unclompeted|
|42|Pengguna dapat logout dari aplikasi.|Sedang|Unclompeted
|43|Pengguna memilih lalu membeli paket yang tersedia|Tinggi|Completed
|44|Pengguna dapat menentukan metode pembayaran|Sedang|Completed
|45|Pengguna dapat melakukan upgrade paket dari yang dimilikinya|Sedang|Completed
|46|Pengguna dapat menghentikan/membatalkan langganan mereka|Sedang|Completed


**Use Case Sisi Manajemen**
| No | Use Case | Prioritas | Status |
|----|----------|-----------|--------|
|1|Admin Netflix mengatur pembaruan konten.|Tinggi|Unclompeted
|2|Netflix mengatur jadwal tayangan untuk film dan serial TV yang mereka miliki, termasuk penentuan tanggal rilis dan pengaturan tayangan berdasarkan geografi atau wilayah tertentu.|Tinggi|Unclompeted
|3|Netflix terus mengembangkan dan memperbarui fitur-fitur platform mereka, seperti mode tontonan offline, kontrol parental, dan fitur interaktif, untuk meningkatkan pengalaman pengguna.|Tinggi|Unclompeted
|4|Admin melindungi privasi pengguna dan mengelola keamanan data.|Tinggi|Completed
|5|Admin Netflix dapat mengelola akun pengguna, termasuk membuat, menghapus, dan mengubah informasi akun.|Sedang|Completed
|6|Manajemen dapat membuat paket-paket baru yang dapat menarik minat pengguna.|Sedang|Completed
|7|Manajemen dapat menyesusaikan harga dan fitur paket yang telah ada|Sedang|Completed
|8|Admin dapat mengelola metode pembayaran yang tersedia bagi pengguna, memastikan kelancaran proses pembayaran dan pembaruan langganan.|Sedang|Completed
|9|Admin Netflix mengelola ketersediaan subtitle dan dubbing untuk berbagai konten, termasuk memastikan kualitas terjemahan dan akurasi subtitle.|Sedang|Unclompeted
|10|Manjemen Netflix menyusun laporan keuangan, analisis anggaran, dan melacak pendapatan serta biaya yang terkait dengan operasional perusahaan.|Rendah|Unclompeted
|11|Admin mengelola kebijakan refund dan mengurus klaim pelanggan terkait masalah pembayaran, gangguan layanan, atau masalah lainnya..|Rendah|Unclompeted
|12|Admin dapat mengelola konten yang ditampilkan di platform Netflix, termasuk menambahkan, menghapus, atau memperbarui konten film, serial TV, dan program lainnya.|Sedang|Unclompeted

**Use Case Sisi Direksi**
| No | Use Case | Prioritas | 
|----|----------|-----------|
|1|Direksi netflix mengelola lisensi konten yang dimiliki oleh perusahaan, termasuk memantau status lisensi, memperbarui perjanjian|Tinggi|
|2|Direksi netflix dapat menggunakan dashboard untuk memantau kinerja keuangan secara real-time, melihat pendapatan, pengeluaran, laba, dan metrik keuangan lainnya.|Tinggi|
|3|Direksi dapat menggunakan dashboard analitik untuk menganalisis data pengguna, termasuk tren penonton, preferensi konten, durasi tayangan, dan perilaku pengguna lainnya.|Sedang|
|4|Direksi dapat menggunakan dashboard untuk memantau pertumbuhan pelanggan, mengamati peningkatan atau penurunan jumlah pelanggan, dan menganalisis faktor-faktor yang memengaruhi pertumbuhan tersebut.|Rendah|
|5|Direksi dapat menggunakan dashboard untuk memantau ketersediaan konten, termasuk jumlah konten yang ditawarkan, keberhasilan lisensi, dan pembaruan konten baru.|Rendah|
|6|Direksi dapat menggunakan dashboard analitik untuk menganalisis persaingan dengan platform streaming lainnya, melihat peringkat, pertumbuhan pengguna, dan strategi bisnis pesaing.|Tinggi|
|7|Direksi dapat menggunakan dashboard untuk menganalisis penggunaan platform Netflix, termasuk durasi tayangan, frekuensi penggunaan, dan interaksi pengguna dengan fitur-fitur platform.|Sedang|
|8|Direksi dapat menggunakan dashboard untuk menganalisis ROI dari investasi perusahaan, termasuk investasi dalam konten, teknologi, pemasaran, dan strategi bisnis lainnya.|Tinggi|
|9| Direksi dapat menggunakan dashboard untuk menganalisis kepuasan pengguna terhadap fitur dan fungsionalitas platform Netflix, melihat umpan balik pengguna, evaluasi penggunaan fitur, dan meningkatkan pengalaman pengguna secara keseluruhan.|Tinggi|
|10|Direksi dapat menggunakan dashboard untuk menganalisis penyebaran dan penerimaan fitur baru oleh pengguna, melihat tingkat adopsi, umpan balik pengguna, dan memahami dampak fitur baru pada pertumbuhan dan retensi pelanggan.|Tinggi|

# No 9
**Link YouTube**

https://youtu.be/x_wBw6o9RcU

# No 10
Pada program saat ini saya masih menggunakan terminal untuk menjalankan program.
![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/10.gif)
